import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class CrossValidation {
	int fold;
	ArrayList<String[]> dataSet;
	ArrayList<ArrayList<String[]>> splitSet;

	/* Shuffle the dataset and split into k folds */
	public CrossValidation(int myFold, ArrayList<String[]> myDataSet) {
		splitSet = new ArrayList<ArrayList<String[]>>();
		fold = myFold;
		dataSet = myDataSet;
		Random random = new Random(66666);
		Collections.shuffle(dataSet, random);
		
		// Split the data set to K folds
		int size = divideUp(this.dataSet.size(), this.fold);
		int curr = 0;
		ArrayList<String[]> list = new ArrayList<String[]>();

		while (curr < this.fold) {
			list = new ArrayList<String[]>();
			if (curr < this.fold - 1) {
				for (int i = 0; i < size; i++) {
					list.add(this.dataSet.get(curr * size + i));
				}
			} else {
				for (int i = 0; i < this.dataSet.size() - curr * size; i++) {
					list.add(this.dataSet.get(curr * size + i));
				}
			}
			splitSet.add(list);
			curr++;
		}
	}
	
	/* Do cross validation and get average accuracy of k times */
	public double doCrossValidation(DecisionTree dt, int fold) {
		double sum = 0;
		for (int i = 0; i < fold; i++) {
			sum += getAccuracy(dt, i);
		}
		return (double)(sum / fold);
	}

	/* Get the accuracy of one time in k-fold cross-validation */
	public double getAccuracy(DecisionTree dt, int i) {
		ArrayList<String[]> testData = splitSet.get(i);

		// Randomly pick 2 folds for pruning
		Random random = new Random(66666);
		ArrayList<Integer> randomSet = new ArrayList<Integer>();
		for (int j = 0; j < fold; j++) {
			if (j != i) {
				randomSet.add(j);
			}
		}
		int rdm = random.nextInt(randomSet.size());
		ArrayList<String[]> prune1 = splitSet.get(randomSet.get(rdm));
		randomSet.remove(rdm);
		int rdm2 = random.nextInt(randomSet.size());
		ArrayList<String[]> prune2 = splitSet.get(randomSet.get(rdm2));
		ArrayList<String[]> pruneData = combineTwoAL(prune1, prune2);

		// Get rest of the folds as training data
		ArrayList<String[]> trainData = new ArrayList<String[]>();
		for (int k = 0; k < splitSet.size(); k++) {
			if (k != i && k != rdm && k != rdm2) {
				trainData.addAll(splitSet.get(k));
			}
		}
		TreeNode root = dt.buildDT(trainData);		
		dt.pruneTree(root, pruneData);	 
		return dt.countAccuracy(root, testData);
	}

	public int divideUp(int a, int b) {
		return (((double) a / (double) b) > (a / b) ? a / b + 1 : a / b);
	}

	/* Use 2 folds of training data to be the pruning data */
	public ArrayList<String[]> combineTwoAL(ArrayList<String[]> al1,
			ArrayList<String[]> al2) {
		ArrayList<String[]> combinedList = new ArrayList<String[]>();
		combinedList.addAll(al1);
		combinedList.addAll(al2);
		return combinedList;
	}
}
