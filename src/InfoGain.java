import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class InfoGain {

	/* get Node IGR for continuous attribute */
	public static Map<Double, String> getConIGR(ArrayList<String[]> dataList,
			int index) {

		Map<Double, String> splitMap = new HashMap<Double, String>();
		int dataLen = dataList.get(0).length;
		int listSize = dataList.size();
		// Sort the data with respect to ith attribute data
		sortAttr(dataList, index);

		// get Info(D) array
		Map<String, Integer> countMap = count(dataList, dataLen - 1);
		ArrayList<Integer> decisionArr = new ArrayList<Integer>();
		for (Iterator<String> it = countMap.keySet().iterator(); it.hasNext();) {
			decisionArr.add(countMap.get(it.next()));
		}

		double infoD = getEntropy(decisionArr);

		// check where to cut
		double maxIGR = Double.MIN_VALUE;
		double maxIG = Double.MIN_VALUE;
		int maxIndex = 0;

		for (int i = 0; i < listSize - 1; i++) {

			if (!dataList.get(i + 1)[index].equals(dataList.get(i)[index])) {
				ArrayList<String[]> leftList = new ArrayList<String[]>(
						dataList.subList(0, i + 1));

				// get left IGR
				Map<String, Integer> leftMap = count(leftList, dataLen - 1);
				ArrayList<Integer> distriLeft = new ArrayList<Integer>();
				for (Iterator<String> it = leftMap.keySet().iterator(); it
						.hasNext();) {
					distriLeft.add(leftMap.get(it.next()));
				}

				double leftEntropy = getEntropy(distriLeft, listSize);
				// get right IGR
				ArrayList<String[]> rightList = new ArrayList<String[]>(
						dataList.subList(i + 1, listSize));
				Map<String, Integer> rightMap = count(rightList, dataLen - 1);
				ArrayList<Integer> distriRight = new ArrayList<Integer>();
				for (Iterator<String> it = rightMap.keySet().iterator(); it
						.hasNext();) {
					distriRight.add(rightMap.get(it.next()));
				}

				double rightEntropy = getEntropy(distriRight, listSize);
				// get infoA
				double infoA = leftEntropy + rightEntropy;
				// get hA
				ArrayList<Integer> hList = new ArrayList<Integer>();
				hList.add(i + 1);
				hList.add(listSize - i - 1);
				double hA = getEntropy(hList);
				// get max IGR & IG (in order to get the threshold)
				double curIGR = getIGR(infoD, infoA, hA);
				double curIG = infoD - infoA;
				maxIGR = curIGR > maxIGR ? curIGR : maxIGR;
				if (curIG > maxIG) {
					maxIG = curIG;
					maxIndex = i;
				}
			}
		}

		splitMap.put(maxIGR, dataList.get(maxIndex)[index]);
		return splitMap;
	}

	/* Basic formula of IGR */
	public static double getIGR(double infoD, double infoA, double hA) {
		if (hA == 0)
			return Double.MIN_VALUE;
		return (double) ((infoD - infoA) / hA);
	}

	private static Map<String, Integer> count(ArrayList<String[]> list,
			int index) {
		Map<String, Integer> countMap = new HashMap<String, Integer>();
		for (String[] data : list) {
			String key = data[index];
			if (!countMap.containsKey(key)) {
				countMap.put(key, 1);
			} else
				countMap.put(key, countMap.get(key) + 1);
		}
		return countMap;
	}

	private static ArrayList<String[]> subset(ArrayList<String[]> list,
			int index, String attribute) {
		ArrayList<String[]> subset = new ArrayList<String[]>();
		for (String[] array : list) {
			if (array[index].equals(attribute)) {
				subset.add(array);
			}
		}
		return subset;
	}

	/** Count IGR of node with discrete value */
	public static double getDisIGR(ArrayList<String[]> dataList, int index) {
		int len = dataList.get(0).length;
		Map<String, Integer> resultCount = count(dataList, len - 1);
		ArrayList<Integer> count = new ArrayList<Integer>();
		for (Iterator<String> it = resultCount.keySet().iterator(); it
				.hasNext();) {
			count.add(resultCount.get(it.next()));
		}
		double rootEntropy = getEntropy(count);

		/* Count the entropy of different values of the attribute */
		double entropy = 0.0;
		// Count the number of records under each value
		Map<String, Integer> attrCount = count(dataList, index);
		ArrayList<Integer> hvCount = new ArrayList<Integer>();
		for (String key : attrCount.keySet()) {
			hvCount.add(attrCount.get(key));
			// get the records matching specific value of this attribute
			ArrayList<String[]> subset = subset(dataList, index, key);

			// Count result number under each value
			Map<String, Integer> subCount = count(subset, len - 1);
			ArrayList<Integer> entroCount = new ArrayList<Integer>();
			for (String sub : subCount.keySet()) {
				entroCount.add(subCount.get(sub));
			}
			entropy += getEntropy(entroCount, dataList.size());
		}
		double HV = getEntropy(hvCount);
		if (HV == 0)
			return Double.MIN_VALUE;
		return (rootEntropy - entropy) / HV;
	}

	// total: size of sub list
	// count: how many count of each labels in the dataset
	public static double getEntropy(ArrayList<Integer> count, int total) {
		double entropy = 0.0;
		int sum = 0;
		for (int i = 0; i < count.size(); i++) {
			sum += count.get(i);
		}
		double current = 0.0;
		for (int i = 0; i < count.size(); i++) {
			if (count.get(i) == 0) {
				current = 0.0;
			} else {
				current = Math.log((double) count.get(i) / sum);
			}
			entropy += (-(double) count.get(i) / sum) * (current / Math.log(2));
		}
		entropy *= (double) sum / total;
		return entropy;
	}

	/* Calculate entropy */
	public static double getEntropy(ArrayList<Integer> count) {
		double entropy = 0.0;
		int sum = 0;
		for (int i = 0; i < count.size(); i++) {
			sum += count.get(i);
		}
		double current = 0.0;
		for (int i = 0; i < count.size(); i++) {
			if (count.get(i) == 0) {
				current = 0.0;
			} else {
				current = Math.log((double) count.get(i) / sum);
			}
			entropy += (-(double) count.get(i) / sum) * (current / Math.log(2));
		}
		return entropy;
	}

	/* sort the attribute with continuous records from smallest to largest */
	public static void sortAttr(ArrayList<String[]> dataList, int index) {
		final int indexFinal = index;
		Collections.sort(dataList, new Comparator<String[]>() {
			public int compare(String[] dataA, String[] dataB) {
				try {
					double a = Double.parseDouble(dataA[indexFinal]);
					double b = Double.parseDouble(dataB[indexFinal]);
					if (a < b) {
						return -1;
					} else if (a > b) {
						return 1;
					} else {
						return 0;
					}
				} catch (Exception e) {
					System.err.println(e);
					return 2;
				}
			}
		});

	}

}
