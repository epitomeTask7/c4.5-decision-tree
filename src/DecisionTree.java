import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.Stack;

public class DecisionTree {
	private ArrayList<String> attribute;
	private ArrayList<ArrayList<String>> attributevalue;
	private int attrLen;
	private ArrayList<Boolean> isDiscrete;

	public DecisionTree(ArrayList<String> attribute,
			ArrayList<ArrayList<String>> attributevalue,
			ArrayList<Boolean> isDiscrete) {
		this.attribute = attribute;
		this.attributevalue = attributevalue;
		this.isDiscrete = isDiscrete;
		this.attrLen = attribute.size();
	}

	/* Build Decision Tree */
	public TreeNode buildDT(ArrayList<String[]> dataList) {
		if (dataList.isEmpty() || dataList == null || dataList.size() == 0)
			return null;

		// check whether it's leaf or not
		if (isPure(dataList)) {
			TreeNode leaf = new TreeNode(dataList.get(0)[attrLen - 1]);
			leaf.setDataCount(dataList.size());
			leaf.setCategory(getMajorCategory(dataList));
			return leaf;
		}

		// if not leaf, build next
		Map<Double, String> curConsValMap = new HashMap<Double, String>();
		Map<Double, String> maxConsValMap = new HashMap<Double, String>();
		double maxIGR = 0;
		int maxIndex = 0;
		for (int i = 0; i < attrLen - 1; i++) {
			// calculate discrete values
			double curIGR = 0;
			if (isDiscrete.get(i)) {
				curIGR = InfoGain.getDisIGR(dataList, i);
				if (curIGR > maxIGR) {
					maxIGR = curIGR;
					maxIndex = i;
				}
			} else {
				curConsValMap = InfoGain.getConIGR(dataList, i);
				for (Iterator<Double> it = curConsValMap.keySet().iterator(); it
						.hasNext();) {
					curIGR = it.next();
				}
				if (curIGR > maxIGR) {
					maxIGR = curIGR;
					maxIndex = i;
					maxConsValMap = curConsValMap;
				}
			}
		}

		// Create Current Node
		TreeNode node = new TreeNode(attribute.get(maxIndex));
		node.setCategory(getMajorCategory(dataList));
		node.setDataCount(dataList.size());
		ArrayList<TreeNode> childNode = new ArrayList<TreeNode>();
		ArrayList<String> nodePath = new ArrayList<String>();

		/* for next step: Get the sub data for children */
		// discrete value
		if (isDiscrete.get(maxIndex)) {
			for (String subAttrVal : attributevalue.get(maxIndex)) {

				ArrayList<String[]> childData = getDisSubData(dataList,
						subAttrVal, maxIndex);
				TreeNode oneChildNode = buildDT(childData);
				if (oneChildNode != null) {
					childNode.add(oneChildNode);
					nodePath.add(subAttrVal);
				}
			}

		} else {
			// consecutive value
			String threshold = maxConsValMap.get(maxIGR);
			ArrayList<String[]> childDataLeft = getConLeftData(dataList,
					threshold, maxIndex);
			TreeNode leftChildNode = buildDT(childDataLeft);
			if (leftChildNode != null) {
				childNode.add(leftChildNode);
				nodePath.add("<= " + threshold);
			}

			ArrayList<String[]> childDataRight = getConRightData(dataList,
					threshold, maxIndex);

			TreeNode rightChildNode = buildDT(childDataRight);
			if (rightChildNode != null) {
				childNode.add(rightChildNode);
				nodePath.add("> " + threshold);
			}
		}

		if (childNode.size() == 0 && nodePath.size() == 0)
			return node;

		node.setPath(nodePath);
		node.setChildNode(childNode);
		return node;
	}

	/* Get sub data of discrete attributes */
	public ArrayList<String[]> getDisSubData(ArrayList<String[]> allData,
			String subAttrVal, int index) {
		ArrayList<String[]> childData = new ArrayList<String[]>();
		for (String[] data : allData) {
			if (subAttrVal.equals(data[index])) {
				childData.add(data);
			}
		}
		return childData;
	}

	/* Get sub-left data of continuous attributes */
	public ArrayList<String[]> getConLeftData(ArrayList<String[]> allData,
			String splitVal, int index) {
		ArrayList<String[]> childDataLeft = new ArrayList<String[]>();
		for (String[] data : allData) {
			try {
				if (Double.parseDouble(data[index]) <= Double
						.parseDouble(splitVal)) {
					childDataLeft.add(data);
				}
			} catch (Exception e) {
				System.err.println(e);
			}
		}
		return childDataLeft;

	}

	/* Get sub-right data of continuous attributes */
	public ArrayList<String[]> getConRightData(ArrayList<String[]> allData,
			String splitVal, int index) {
		ArrayList<String[]> childDataRight = new ArrayList<String[]>();
		for (String[] data : allData) {
			try {
				if (Double.parseDouble(data[index]) > Double
						.parseDouble(splitVal)) {
					childDataRight.add(data);
				}
			} catch (Exception e) {
				System.err.println(e);
			}
		}
		return childDataRight;

	}

	/* check whether it's leaf or not */
	public boolean isPure(ArrayList<String[]> dataList) {
		// assumption: dataList is not null or empty
		String preDeci = dataList.get(0)[attrLen - 1];
		for (String[] data : dataList) {
			if (!data[attrLen - 1].equals(preDeci)) {
				return false;
			}
		}
		return true;
	}

	/* Get majority category of the node according to the dataList falling into it*/
	public String getMajorCategory(ArrayList<String[]> dataList) {
		int index = dataList.get(0).length - 1;
		String label = "";
		Map<String, Integer> countMap = new HashMap<String, Integer>();
		for (String[] row : dataList) {
			String key = row[index];
			if (!countMap.containsKey(key)) {
				countMap.put(key, 1);
			} else
				countMap.put(key, countMap.get(key) + 1);
		}
		int maxCount = Integer.MIN_VALUE;
		for (String key : countMap.keySet()) {
			if (countMap.get(key) > maxCount) {
				maxCount = countMap.get(key);
				label = key;
			}
		}
		return label;
	}

	// Get the data list in each of the node
	public Map<TreeNode, ArrayList<String[]>> getNodeData(TreeNode root,
			ArrayList<String[]> testData) {

		Map<TreeNode, ArrayList<String[]>> nodeMap = new HashMap<TreeNode, ArrayList<String[]>>();
		Stack<TreeNode> stack = new Stack<TreeNode>();

		nodeMap.put(root, testData);
		stack.push(root);

		while (!stack.isEmpty()) {
			TreeNode node = stack.pop();
			if (node.getChildNode().size() > 0) {
				getNodeDataHelper(nodeMap, node, nodeMap.get(node));
				for (TreeNode child : node.getChildNode()) {
					stack.push(child);
				}
			}
		}

		return nodeMap;
	}

	public void getNodeDataHelper(Map<TreeNode, ArrayList<String[]>> map,
			TreeNode parent, ArrayList<String[]> testData) {
		int curr = 0;
		int index = attribute.indexOf(parent.getName());

		for (TreeNode node : parent.getChildNode()) {
			if (isDiscrete.get(index)) {
				ArrayList<String[]> subset = getDisSubData(testData, parent
						.getPath().get(curr), index);
				map.put(node, subset);

			} else {
				String splitValue = parent.getPath().get(0).split(" ")[1];
				ArrayList<String[]> subset = getConSubData(testData,
						splitValue, index).get(curr);
				map.put(node, subset);

			}
			curr++;
		}
	}


	
	/* Print the tree */
	public void printTree(TreeNode root) {
		if (root == null) {
			return;
		}
		ArrayList<ArrayList<String>> res = new ArrayList<ArrayList<String>>();
		Queue<TreeNode> helper = new LinkedList<TreeNode>();
		helper.offer(root);

		int levelCount = 0;
		while (!helper.isEmpty()) {
			ArrayList<String> level = new ArrayList<String>();
			int size = helper.size();
			levelCount++;
			System.err.println("Number of Nodes in Level " + levelCount + ": "
					+ size);
			for (int i = 0; i < size; i++) {
				TreeNode cur = helper.poll();
				String curName = cur.getName();
				System.err.print("NodeName = (( " + curName
						+ " )), MajorCategory = {{ " + cur.getCategory()
						+ " }}, dataCount = // " + cur.getDataCount() + " //, "
						+ " Error = || " + cur.getErrorCount() + " ||");
				if (!cur.getPath().isEmpty() || cur.getPath().size() != 0) {
					System.err.print("Path: [");
					for (String p : cur.getPath()) {
						System.err.print(p + ", ");
					}
					System.err.print("], ");
				}
				level.add(curName);
				if (!cur.getChildNode().isEmpty()) {
					System.err.print("ChildCount: ["
							+ cur.getChildNode().size() + "], Childname: [");
					for (TreeNode child : cur.getChildNode()) {
						if (child != null) {
							helper.offer(child);
							System.err.print(child.getName() + ", ");
						}
					}
					System.err.print("]");
				}

				System.err.print(" >>>>> ");
			}
			res.add(level);
			System.err.println();
			System.err
					.println(">-----------------------------------------------------<");
		}

	}

	/*
	 * Tree pruning method implementing reduce error pruning algorithm
	 */
	public void pruneTree(TreeNode root, ArrayList<String[]> pruneData) {
		Map<TreeNode, ArrayList<String[]>> map = getNodeData(root, pruneData);
		// traverse the tree and count the error at each node for the pruneData
		// update error count at each node
		populateErrorCount(map);
		// traverse from left to right bottom up(post order and check
		// intermediate node
		// create a postorder iterator
		pruneTree(root);
	}

	/*
	 * check tree bottom up and prune when error count is less than error count
	 * of childs
	 */
	private int pruneTree(TreeNode root) {
		if (root.getChildNode().isEmpty())
			return root.getErrorCount();
		// recurse for all the childs
		ArrayList<TreeNode> childs = root.getChildNode();
		int childError = 0;
		for (TreeNode treeNode : childs) {
			childError += pruneTree(treeNode);
		}
		int rootError = root.getErrorCount();
		if (rootError > childError) {
			// update error count at this level
			root.setErrorCount(childError);
		} else {
			// cut the subtree below this level
			root.setName(root.getCategory());
			root.setChildNode(new ArrayList<TreeNode>());
		}
		return root.getErrorCount();
	}

	/*
	 * populates every treenode with the error count for the validation data
	 */
	private void populateErrorCount(Map<TreeNode, ArrayList<String[]>> map) {
		Set<TreeNode> nodes = map.keySet();
		for (TreeNode treeNode : nodes) {
			ArrayList<String[]> recordsPassingAtThisNode = map.get(treeNode);
			for (String[] data : recordsPassingAtThisNode) {
				if (!data[data.length - 1].equals(treeNode.getCategory())) {
					treeNode.setErrorCount(treeNode.getErrorCount() + 1);
				}
			}
		}

	}


	// Given a tree and test data, count the accuracy
	public double countAccuracy(TreeNode root, ArrayList<String[]> testData) {
		Map<TreeNode, ArrayList<String[]>> map = getNodeData(root, testData);
		ArrayList<int[]> rightWrong = new ArrayList<int[]>();
		for (TreeNode node : map.keySet()) {
			if (node.getChildNode().size() == 0) {
				ArrayList<String[]> nodeData = map.get(node);
				int correct = 0;
				for (String[] data : nodeData) {
					if (data[data.length - 1].equals(node.getName())) {
						correct++;
					}
				}
				rightWrong
						.add(new int[] { correct, nodeData.size() - correct });
			}
		}
		int right = 0, wrong = 0;
		for (int[] record : rightWrong) {
			right += record[0];
			wrong += record[1];
		}
		return (double) right / (right + wrong);
	}

	public ArrayList<ArrayList<String[]>> getConSubData(
			ArrayList<String[]> allData, String splitVal, int index) {
		ArrayList<ArrayList<String[]>> splitArr = new ArrayList<ArrayList<String[]>>();
		ArrayList<String[]> childDataLeft = new ArrayList<String[]>();
		ArrayList<String[]> childDataRight = new ArrayList<String[]>();
		for (String[] data : allData) {
			try {
				if (Double.parseDouble(data[index]) <= Double
						.parseDouble(splitVal)) {
					childDataLeft.add(data);
				} else {
					childDataRight.add(data);
				}
			} catch (Exception e) {
				System.err.println(e);
			}
		}
		splitArr.add(childDataLeft);
		splitArr.add(childDataRight);
		return splitArr;

	}

	public ArrayList<String> getPredictRes(TreeNode root,
			ArrayList<String[]> testData) {
		ArrayList<String> res = new ArrayList<String>();

		for (String[] data : testData) {
			res.add(getPredictHelper(root, data));
		}
		return res;
	}

	public String getPredictHelper(TreeNode root, String[] data) {
		String label = "";
		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(root);

		while (!stack.isEmpty()) {
			TreeNode node = stack.pop();
			// System.err.println("node.name = " + node.getName());
			int index = attribute.indexOf(node.getName());
			// System.err.println("attribute index = " + index);
			if (index == -1) {
				label = node.getName();
				break;
			}
			if (node.getPath() != null || node.getPath().size() > 0) {
				if (isDiscrete.get(index)) {
					if (node.getPath().indexOf(data[index]) != -1) {
						stack.push(node.getChildNode().get(
								node.getPath().indexOf(data[index])));
					} else {
						return node.getCategory();
					}
				} else {
					try {
						double threshold = Double.parseDouble(node.getPath()
								.get(1).substring(1));
						double curVal = Double.parseDouble(data[index]);
						stack.push(curVal <= threshold ? node.getChildNode()
								.get(0) : node.getChildNode().get(1));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else {
				return node.getCategory();
			}
		}
		return label;
	}

	public TreeNode buildFinalModel(ArrayList<String[]> trainData, int fold) {
		ArrayList<ArrayList<String[]>> splitSet = new ArrayList<ArrayList<String[]>>();
		Collections.shuffle(trainData, new Random(66666));

		// Split the data set to K folds
		int size = divideUp(trainData.size(), fold);
		int curr = 0;
		ArrayList<String[]> list = new ArrayList<String[]>();

		while (curr < fold) {
			list = new ArrayList<String[]>();
			if (curr < fold - 1) {
				for (int i = 0; i < size; i++) {
					list.add(trainData.get(curr * size + i));
				}
			} else {
				for (int i = 0; i < trainData.size() - curr * size; i++) {
					list.add(trainData.get(curr * size + i));
				}
			}
			splitSet.add(list);
			curr++;
		}

		// Randomly pick 2 folds for pruning
		Random random = new Random(66666);
		ArrayList<Integer> randomSet = new ArrayList<Integer>();
		for (int j = 0; j < fold; j++) {

			randomSet.add(j);

		}
		int rdm = random.nextInt(randomSet.size());
		ArrayList<String[]> prune1 = splitSet.get(randomSet.get(rdm));
		randomSet.remove(rdm);
		int rdm2 = random.nextInt(randomSet.size());
		ArrayList<String[]> prune2 = splitSet.get(randomSet.get(rdm2));
		ArrayList<String[]> pruneData = combineTwoAL(prune1, prune2);

		// Get rest of the folds as building data
		ArrayList<String[]> buildData = new ArrayList<String[]>();
		for (int k = 0; k < splitSet.size(); k++) {
			if (k != rdm && k != rdm2) {
				buildData.addAll(splitSet.get(k));
			}
		}

		// Build Tree
		TreeNode root = buildDT(trainData);
		// Pruning Tree
		pruneTree(root, pruneData);
		return root;

	}

	public int divideUp(int a, int b) {
		return (((double) a / (double) b) > (a / b) ? a / b + 1 : a / b);
	}

	public ArrayList<String[]> combineTwoAL(ArrayList<String[]> al1,
			ArrayList<String[]> al2) {
		ArrayList<String[]> combinedList = new ArrayList<String[]>();
		combinedList.addAll(al1);
		combinedList.addAll(al2);
		return combinedList;
	}

}
