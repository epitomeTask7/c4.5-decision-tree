import java.io.*;
import java.util.*;
import java.util.regex.*;

public class DataSet {
	public static final String patternString = "@attribute(.*)[{](.*?)[}]";
	public ArrayList<String> attribute;
	public ArrayList<ArrayList<String>> attributevalue;
	public ArrayList<String[]> data;
	public ArrayList<Boolean> isDiscrete;

	public DataSet() {
		attribute = new ArrayList<String>();
		attributevalue = new ArrayList<ArrayList<String>>();
		data = new ArrayList<String[]>();
		isDiscrete = new ArrayList<Boolean>();
	}

	/* METHOD: read data from file */
	public void readARFF(File file) {
		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			Pattern pattern = Pattern.compile(patternString);
			while ((line = br.readLine()) != null) {
				Matcher matcher = pattern.matcher(line);
				if (matcher.find()) {
					attribute.add(matcher.group(1).trim());
					isDiscrete.add(true);
					String[] values = matcher.group(2).split(",");
					ArrayList<String> al = new ArrayList<String>(values.length);
					for (String value : values) {
						al.add(value.trim());
					}
					attributevalue.add(al);
				} else if (line.startsWith("@attribute")) {
					String[] attrName = line.split(" ");
					attribute.add(attrName[1]);
					isDiscrete.add(false);
					attributevalue.add(new ArrayList<String>());
				} else if (line.startsWith("@data")) {
					while ((line = br.readLine()) != null) {
						if (line == "")
							continue;
						String[] row = line.split(",");
						data.add(row);
					}
				} else {
					continue;
				}
			}
			br.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
