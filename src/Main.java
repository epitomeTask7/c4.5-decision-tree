import java.io.File;
import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
		int fold = Integer.parseInt(args[0]);
		/* 1. Product Selection */
		System.out.println("1. Product Selection: ");
		DataSet dataSet1 = new DataSet();
		dataSet1.readARFF(new File(args[1]));

		// Cross validation
		CrossValidation cv1 = new CrossValidation(fold, dataSet1.data);
		DecisionTree dt1 = new DecisionTree(dataSet1.attribute,
				dataSet1.attributevalue, dataSet1.isDiscrete);
		System.out.println("1.1) Average accuracy of Product Selection in "
				+ fold + " fold Cross Validation is "
				+ cv1.doCrossValidation(dt1, fold));

		// Predict Data
		System.out.println("1.2) Predict result of Product Selection is: ");
		DataSet testData1 = new DataSet();
		testData1.readARFF(new File(args[2]));
		ArrayList<String> predict1 = dt1.getPredictRes(
				dt1.buildFinalModel(dataSet1.data, fold), testData1.data);

		for (int i = 0; i < predict1.size(); i++) {
			System.out.println("     " + (i + 1) + ": " + predict1.get(i));
		}

		System.out.println();
		/* 2. Product Introduction */
		System.out.println("2. Product Introduction: ");
		DataSet dataSet2 = new DataSet();
		dataSet2.readARFF(new File(args[3]));

		// Cross Validation
		CrossValidation cv2 = new CrossValidation(fold, dataSet2.data);
		DecisionTree dt2 = new DecisionTree(dataSet2.attribute,
				dataSet2.attributevalue, dataSet2.isDiscrete);
		System.out.println("2.1) Average accuracy of Product Introduction in "
				+ fold + " fold Cross Validation is "
				+ cv2.doCrossValidation(dt2, fold));

		// Predict Data
		System.out.println("2.2) Predict result of Product Introduction is: ");
		DataSet testData2 = new DataSet();
		testData2.readARFF(new File(args[4]));
		ArrayList<String> predict2 = dt2.getPredictRes(
				dt2.buildFinalModel(dataSet2.data, fold), testData2.data);

		for (int i = 0; i < predict2.size(); i++) {
			System.out.println("     " + (i + 1) + ": " + predict2.get(i));
		}

	}

}
