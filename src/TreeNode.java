import java.util.ArrayList;

public class TreeNode {
	private String name;
	private ArrayList<String> path;
	private ArrayList<TreeNode> childNode;
	private int dataCount;
	private String category;
	private int errorCount;
	
	public TreeNode(String name) {
		this.name = name;
		this.childNode = new ArrayList<TreeNode>();
		this.path = new ArrayList<String>();
		this.dataCount = 0;
		this.category = "";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<String> getPath() {
		return path;
	}

	public void setPath(ArrayList<String> path) {
		this.path = path;
	}

	public ArrayList<TreeNode> getChildNode() {
		return childNode;
	}

	public void setChildNode(ArrayList<TreeNode> childNode) {
		this.childNode = childNode;
	}

	public int getDataCount() {
		return dataCount;
	}

	public void setDataCount(int dataCount) {
		this.dataCount = dataCount;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(Integer errorCount) {
		this.errorCount = errorCount;
	}
}
