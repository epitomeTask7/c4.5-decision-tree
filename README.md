[Team 7, Epitome]
Team members: 
Amey Jain
Bingyu Zhang
Jhalak Goyal
Jiali Chen
Qianwen Li

[Project description]
This project is to implement a Decision Tree Algorithm C4.5 to build a model in order to predict the input dataset. We used C4.5 which is an advanced version of ID3 to build up the tree, and then used a pruning algorithm -- Reduced Error Pruning to prune the tree. Lastly, with a high average accuracy result from the cross validation, we predicted for the testing dataset. 

[Usage]
Execute the program as commands below:
java Main <#_of_fold> <production_selection_train_data> <production_selection_test_data> <production_introduction_train_data> <production_introduction_test_data>
For example:
java Main 10 ./trainProdSelection.arff ./testProdSelection.arff ./trainProdIntro.binary.arff ./testProdIntro.binary.arff

[Reference links / materials]

1. http://blog.csdn.net/xuxurui007/article/details/18045943

2. http://www.cnblogs.com/zhangchaoyang/articles/2842490.html

3. Ruggieri, S. (2002). Efficient C4. 5 [classification algorithm]. Knowledge and Data Engineering, IEEE Transactions on, 14(2), 438-444.

4. http://courses.cs.washington.edu/courses/cse473/98wi/slides/0311-learning/sld017.htm

5. http://www.academia.edu/4375403/Decision_Tree_Analysis_on_J48_Algorithm_for_Data_Mining
 
6. http://scikit-learn.org/stable/modules/tree.html#tree-algorithms-id3-c4-5-c5-0-and-cart

7. http://rulequest.com/see5-comparison.html.

8. http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.35.5428&rep=rep1&type=pdf

9. http://www.uni-weimar.de/medien/webis/teaching/lecturenotes/machine-learning/unit-en-decision-trees-basics.pdf#algorithm-tdidt

10. https://www.cs.princeton.edu/courses/archive/spring07/cos424/papers/mitchell-dectrees.pdf